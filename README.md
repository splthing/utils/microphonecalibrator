#  microphonecalibrator

A Processing app to compare the frequency responses of a reference ("ref") mic and another mic ("target"), then export the difference to Octave for filter coefficient generation.

Octave bit doesn't work quite yet.  



## Requirements

### Software
* [Processing 3](https://processing.org/download)
* [GNU Octave](https://www.gnu.org/software/octave/index) (CLI - don't need GUI version)

### Processing Libraries (install with Contribution Manager)
* ControlP5
* grafica

The jsyn Java library is needed, but already included in the sketch directory.

## Installation

clone into your processing sketch folder. The sketch files must be inside a folder called "microphonecalibrator"

Once you have the initial clone, remove the custom.pde file and any sweeps/settings from git tracking, so they don't get overwritten by subsequent pulls.

Commands:

```cd ~/Documents/Processing```  

```git clone https://gitlab.com/splthing/utils/microphonecalibrator.git microphonecalibrator```

```cd microphonecalibrator```

```git rm --cached custom.pde```

```git rm --cached data/```

## Setup

This talks to the ESP32 through the same serial port as "faart". Connect a USB-Serial device to the ESP (defaults ESP TX=GPIO2, ESP RX=GPIO17) and to the computer.

## Configuration

Change settings in the custom.pde file. Set up the audio device name and output you want to use to generate the sweeps. Even if the DEVICE_NAME is wrong, running the sketch wil list all the devices on the system. You can copy and paste the name from the Processing console.

SERIAL_PORT is the USB-Serial device mentioned earlier.

## Usage

The chart shows three traces:
* The reference sweep (red)
* The target sweep (blue)
* The difference between the two (green)

The input buttons switch the ESP32 analysis between the two ADC inputs.

Calibration tone turns on and off the calibration tone (duh)  

Gain will change the gain for channel 1 only. Use this to set a reasonable dBFS value before referencing to 94dB SPL.

Set ref 94dB will store the current raw sample level as a reference for 94dBZ. 

The three red buttons start a sweep, save and load the files for the reference mic. Blue ones do the same for the target mic.

The ref sweep is stored in the sketch "data" directory. Target sweeps can be saved and loaded from wherever.

The red sliders allow you to pick frequency ranges included in the filters. The idea behind this is to allow Octave to curve-fit better to the higher and lower extremes if we ignore all the flat stuff in the middle. Turns out we also need to cherry-pick frequencies within these ranges as well, rather than just including the whole thing. This will be later.

Calc coeff will process the filter in octave. It is not sent to the ESP by default, you can find the serial sending command at the end of the generateOctaveParams function in the octave tab.

Ignore the other yellow button its stupid 

## How I think it would be used

This could all be complete bullshit but here's my thought process. The "test chamber" is a speaker-in-a-box.

1. The ref mic is plugged into Input 2 of the ESP.
2. Exercise the ref mic to 94dBZ by a pistonphone.
3. Switch to Input 2 and hit the "set ref" button.
4. Move the ref mic to the test chamber.
5. Switch on the calibration tone.
6. Adjust level until 94dB is shown.

That's the SPL part. Now we will be at 94dB SPL every time the generator is turned on.

7. "Sweep ref" and then "Save ref".
8. Put the target mic in the test chamber.
9. Switch on the calibration tone.
10. Adjust target mic gain (how?) for 94dBZ SPL.
11. "Sweep tgt" and then "Save tgt".


