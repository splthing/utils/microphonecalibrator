ControlP5 cp5;

Button recordRef, sweepStop, saveRef, loadRef, ref2cal, recordTarget, saveTarget, exportCSV, loadTarget, level94dB, calcCoeff, showCoeff;
Button setInput1, ch1_gainUp, ch1_gainDown, ch1_autoGain;
Button setInput2, ch2_gainUp, ch2_gainDown, ch2_autoGain;
Textfield ch1_gainManual, ch2_gainManual;
Button ESPReboot;
Toggle calibrationTone, applyCal;
Slider calibrationLevel;
Textarea consoleTextArea;
Group analysis;
Range LFRange, HFRange;

// set up GUI

CallbackListener cb = new CallbackListener() {
  public void controlEvent(CallbackEvent theEvent) {
    if (theEvent.getController()==ch1_gainManual) {
      ch1_gainManual.setText("");
    }
        if (theEvent.getController()==ch2_gainManual) {
      ch2_gainManual.setText("");
    }
    //println (theEvent.toString());
  }
};


void cp5init() {
  cp5 = new ControlP5(this);
  // change the default font to Verdana
  PFont p = createFont("Verdana", 20); 
  ControlFont font = new ControlFont(p);

  // no uppercase
  Label.setUpperCaseDefault(false);

  // change the original colors
  cp5.setColorForeground(0xffaa0000);
  cp5.setColorBackground(0xff660000); 
  cp5.setColorActive(0xffff0000);
  cp5.setFont(font);

  cp5.setAutoDraw(false);      // handle the redrawnig ourselves

  // channel 1 group (

  Group ch1 = cp5.addGroup("ch1 (Capsule)")
    .setSize(180, 160)
    .setBarHeight(30)
    .setPosition (10, 320)
    .setBackgroundColor(color(255, 50))
    .disableCollapse()
    ;

  ch1.setPosition (ch1.getPosition()[0], ch1.getPosition()[1]+ch1.getBarHeight());    // bump down by bar height

  ch1_gainDown= cp5.addButton ("-1")
    .setPosition (10, 10)
    .setSize(30, 40)
    .setColorForeground(0xff00aaaa)
    .setColorBackground(0xff006666)
    .setColorActive(0xff00ffff)
    .setGroup (ch1)
    .setLabel ("-");
  ; 

  ch1_gainUp= cp5.addButton ("+1")
    .setPosition (140, 10)
    .setSize(30, 40)
    .setColorForeground(0xff00aaaa)
    .setColorBackground(0xff006666)
    .setColorActive(0xff00ffff)
    .setGroup (ch1)
    .setLabel ("+");
  ;
  ch1_autoGain= cp5.addButton ("auto1")
    .setPosition (10, 60)
    .setSize(160, 40)
    .setColorForeground(0xff00aaaa)
    .setColorBackground(0xff006666)
    .setColorActive(0xff00ffff)
    .setGroup (ch1)
    .setLabel ("Auto");
  ;
  ch1_gainManual = cp5.addTextfield("gainmanual1")
    .setPosition (50, 10)
    .setSize (80, 40)
    .setColorForeground(0xff00aaaa)
    .setColorBackground(0xff006666)
    .setColorActive(0xff00ffff)
    .setLabel(" ")
    .setGroup (ch1)
    .onPress (cb)
    ;
  ch1_gainManual.setLabelVisible(false);

  setInput1= cp5.addButton ("analyze1")
    .setPosition (10, 110)
    .setSize(160, 40)
    .setColorForeground(0xffaa00aa)
    .setColorBackground(0xff660066)
    .setColorActive(0xffff00ff)
    .setGroup (ch1)
    .setLabel ("Analyze 1") 
    ; 

  // channel 2 controls

  Group ch2 = cp5.addGroup("ch2 (XLR)")
    .setSize(180, 160)
    .setBarHeight(30)
    .setPosition (200, 320)
    .setBackgroundColor(color(255, 50))
    .disableCollapse()
    ;

  ch2.setPosition (ch2.getPosition()[0], ch2.getPosition()[1]+ch2.getBarHeight());    // bump down by bar height

  ch2_gainDown= cp5.addButton ("-2")
    .setPosition (10, 10)
    .setSize(30, 40)
    .setColorForeground(0xff00aaaa)
    .setColorBackground(0xff006666)
    .setColorActive(0xff00ffff)
    .setGroup (ch2)
    .setLabel ("-")
    ; 

  ch2_gainUp= cp5.addButton ("+2")
    .setPosition (140, 10)
    .setSize(30, 40)
    .setColorForeground(0xff00aaaa)
    .setColorBackground(0xff006666)
    .setColorActive(0xff00ffff)
    .setGroup (ch2)
    .setLabel ("+")
    ;
  ch2_autoGain= cp5.addButton ("auto2")
    .setPosition (10, 60)
    .setSize(160, 40)
    .setColorForeground(0xff00aaaa)
    .setColorBackground(0xff006666)
    .setColorActive(0xff00ffff)
    .setGroup (ch2)
    .setLabel ("auto")
    ;
  ch2_gainManual = cp5.addTextfield("gainmanual2")
    .setPosition (50, 10)
    .setSize (80, 40)
    .setColorForeground(0xff00aaaa)
    .setColorBackground(0xff006666)
    .setColorActive(0xff00ffff)
    .setLabel(" ")
    .setGroup (ch2)
        .onPress (cb)
    ;
  ch1_gainManual.setLabelVisible(false);

  setInput2= cp5.addButton ("analyze2")
    .setPosition (10, 110)
    .setSize(160, 40)
    .setColorForeground(0xffaa00aa)
    .setColorBackground(0xff660066)
    .setColorActive(0xffff00ff)
    .setGroup (ch2)
    .setLabel ("analyze")
    ; 

  // analysis

  analysis = cp5.addGroup ("analysis")
    .setPosition (10, 550)
    .setSize (370, 160)
    .setBarHeight (30)
    .setBackgroundColor(color(255, 50))
    .disableCollapse()
    ;
  level94dB= cp5.addButton ("Set ref 94dBZ SPL")
    .setPosition (10, 110)
    .setSize(350, 40)
    .setColorForeground(0xff00aaaa)
    .setColorBackground(0xff006666)
    .setColorActive(0xff00ffff)
    .setGroup (analysis);
    ; 

  ESPReboot= cp5.addButton ("espreboot")
    .setPosition (720, 320)
    .setSize(160, 40)
    .setColorForeground(0xffaa00aa)
    .setColorBackground(0xff660066)
    .setColorActive(0xffff00ff)
    ; 

  calibrationTone= cp5.addToggle ("Calibration Tone")
    .setPosition (510, 370)
    .setSize(330, 40)
    .setColorForeground(0xff009900)
    .setColorBackground(0xff006600)
    .setColorActive(0xff00cc00)
    .setCaptionLabel("Tone");

  ;
  calibrationTone.getCaptionLabel().alignY(ControlP5.CENTER);
  calibrationTone.getCaptionLabel().alignX(ControlP5.CENTER);

  calibrationLevel = cp5.addSlider("calibration tone level")
    .setPosition (360, 370)
    .setSize (400, 40)
    .setMin(0.0)
    .setMax(1.0)
    .setColorForeground(0xff009900)
    .setColorBackground(0xff006600)
    .setColorActive(0xff00aa00)
    .setCaptionLabel("")

    ;

  // save settings if we adjust the calibration level slider
  calibrationLevel.onRelease(new CallbackListener() {
    public void controlEvent (CallbackEvent theEvent) {
      ss();
    }
  }
  );


  recordRef = cp5.addButton ("sweep ref")
    .setPosition (410, 520)
    .setSize(160, 40)
    .setColorForeground(0xffaa0000)
    .setColorBackground(0xff660000)
    .setColorActive(0xffff0000)
    ;

  saveRef = cp5.addButton ("save ref")
    .setPosition (580, 520)
    .setSize(160, 40)
    .setColorForeground(0xffaa0000)
    .setColorBackground(0xff660000)
    .setColorActive(0xffff0000)
    ;

  loadRef = cp5.addButton ("load ref")
    .setPosition (750, 520)
    .setSize(160, 40)
    .setColorForeground(0xffaa0000)
    .setColorBackground(0xff660000)
    .setColorActive(0xffff0000)
    ; 

  ref2cal = cp5.addButton ("ref2cal")
    .setPosition (920, 520)
    .setSize(160, 40)
    .setColorForeground(0xffaa0000)
    .setColorBackground(0xff660000)
    .setColorActive(0xffff0000)
    ; 

  applyCal = cp5.addToggle ("apply Cal")
    .setPosition (1090, 520)
    .setSize(160, 40)
    .setColorForeground(0xffaa0000)
    .setColorBackground(0xff660000)
    .setColorActive(0xffff0000)
    ;   

  applyCal.getCaptionLabel().alignY(ControlP5.CENTER);
  applyCal.getCaptionLabel().alignX(ControlP5.CENTER);
  recordTarget = cp5.addButton ("sweep tgt")
    .setPosition (410, 570)
    .setSize(160, 40)
    .setColorForeground(0xff0000aa)
    .setColorBackground(0xff000066)
    .setColorActive(0xff0000ff)
    ;

  saveTarget = cp5.addButton ("save tgt")
    .setPosition (580, 570)
    .setSize(160, 40)
    .setColorForeground(0xff0000aa)
    .setColorBackground(0xff000066)
    .setColorActive(0xff0000ff)
    ;

  loadTarget = cp5.addButton ("load tgt")
    .setPosition (750, 570)
    .setSize(160, 40)
    .setColorForeground(0xff0000aa)
    .setColorBackground(0xff000066)
    .setColorActive(0xff0000ff)
    ;

  calcCoeff = cp5.addButton ("calc coeff")
    .setPosition (410, 620)
    .setSize(160, 40)
    .setColorForeground(0xffaaaa00)
    .setColorBackground(0xff666600)
    .setColorActive(0xffffff00)
    ;

  LFRange = cp5.addRange ("lfrange")
    .setPosition (580, 620)
    .setSize (160, 40)
    .setRange (0, 1000)
    .setDecimalPrecision (0)
    .setLabel(" ")
    .setLabelVisible(false);
  ;

  HFRange = cp5.addRange ("hfrange")
    .setPosition (750, 620)
    .setSize (160, 40)
    .setRange (750, 20000)
    .setRangeValues (6000, 20000)
    .setDecimalPrecision (0)
    .setLabel(" ")
    .setLabelVisible(false)
    ;
  showCoeff = cp5.addButton ("show coeff")
    .setPosition (410, 670)
    .setSize(160, 40)
    .setColorForeground(0xffaaaa00)
    .setColorBackground(0xff666600)
    .setColorActive(0xffffff00)
    ;

  sweepStop = cp5.addButton ("sweepstop")
    .setPosition (width-170, 420)
    .setSize(160, 90)
    ;


  consoleTextArea = cp5.addTextarea("txt")
    .setSize(500, 210)
    .setFont(createFont("Monaco", 10))
    .setLineHeight(14)
    .setColor(color(0, 255, 1))
    .setColorBackground(color(0, 0, 0.05))
    ;

  consoleTextArea.setPosition (10, height-consoleTextArea.getHeight()-10);
}






// handle GUI interactions

void controlEvent(ControlEvent theEvent) {
  if (theEvent.isFrom(recordRef)) {
    println ("sweep");
    fPoints = pointsRef;
    clearPoints(fPoints);
    clearPoints(pointsDiff);
    calibrationTone.setValue(0);    
    sweepOn=true;
    thread("sweepThread");
  } else if (theEvent.isFrom(sweepStop)) {
    sineOsc.amplitude.set(0);
    sweepOn=false;
  } else if (theEvent.isFrom(saveRef)) {
    // save ref points to json file
    saveSweep ("itsafuckingreference", dataPath(REF_FILENAME), pointsRef);
  } else if (theEvent.isFrom(loadRef)) {
    // load ref points
    pointsRef = loadSweep(dataPath(REF_FILENAME), true);
    calcDiff();
  } else if (theEvent.isFrom(ref2cal)) {
    // reference the ref sweep to 0 instead of REF_DB (typically 94) and save it
    clearPoints(pointsCal);
    for (int i=0; i<SWEEP_POINTS; i++) {
      pointsCal.add(pointsRef.get(i).getX(), REF_DB-pointsRef.get(i).getY());
    }
    saveSweep ("calibration", dataPath(CAL_FILENAME), pointsCal);
  } else if (theEvent.isFrom(applyCal)) {
  } else if (theEvent.isFrom(recordTarget)) {
    println ("sweep target");
    fPoints = pointsTarget;
    clearPoints(fPoints);
    clearPoints(pointsDiff);
    calibrationTone.setValue(0);
    sweepOn=true;
    thread("sweepThread");
  } else if (theEvent.isFrom(saveTarget)) {
    // save target points to json file
    selectOutput("save target as json", "saveTarget");
  } else if (theEvent.isFrom(loadTarget)) {
    // load target
    selectInput("load target sweep", "loadTarget");
  } else if (theEvent.isFrom(level94dB)) {
    // load target
    myPort.write(String.format ("@dbref=%f\n", REF_DB));
  } else if (theEvent.isFrom(calibrationTone)) {
    // load target
    if (calibrationTone.getValue()==1.0) {
      sineOn();
      sineOsc.frequency.set(CALIB_FREQ);      // 251.2????
    } else {
      sineOff();
    }
  } else if (theEvent.isFrom(calibrationLevel)) {
    sineAmp=calibrationLevel.getValue();
    sineUpdate();
  } else if (theEvent.isFrom(calcCoeff)) {
    generateOctaveParams();
  } else if (theEvent.isFrom (LFRange)) {
    LFLow = findClosest(iec_f, LFRange.getArrayValue()[0]);
    LFHigh = findClosest(iec_f, LFRange.getArrayValue()[1]);
  } else if (theEvent.isFrom (HFRange)) {
    HFLow = findClosest(iec_f, HFRange.getArrayValue()[0]);
    HFHigh = findClosest(iec_f, HFRange.getArrayValue()[1]);
  } else if (theEvent.isFrom (showCoeff)) {
    sa = new SecondApplet();
    PApplet.runSketch(args, sa);
    PVector windowLocation = getWindowLocation();
    sa.pos ((int)windowLocation.x, (int)windowLocation.y);
    sa.showCoeff();
  } 
  
  //channel 1 gain  
  
  else if (theEvent.isFrom (ch1_gainUp)) {
    if (gain1!=-666) {
      float tempGain=gain1 +0.1;
      String out = String.format("@gain1=%.1f\n", tempGain);
      println (String.format ("OUT: %s", out));
      myPort.write(out);
    }
  } else if (theEvent.isFrom (ch1_gainDown)) {
    if (gain1!=-666) {
      float tempGain=gain1-0.1;
      String out = String.format("@gain1=%.1f\n", tempGain);
      println (String.format ("OUT: %s", out));
      myPort.write(out);
    }
  } else if (theEvent.isFrom (ch1_gainManual)) {
    try {
      float manualValue=Float.parseFloat (ch1_gainManual.getStringValue());
      if (manualValue >=0 && manualValue <=38) {
        manualValue = Math.round(manualValue*10)/10.0f;      // round to nearest 0.1
        dbg (String.format ("manual value: %.1f\n", manualValue));
        String out = String.format("@gain1=%.1f\n", manualValue);
        myPort.write(out);
      } else {
        dbg ("gain can only be 0-38dB\n");
      }
      ch1_gainManual.setFocus(false);
    } 
    catch (NumberFormatException e) {
      dbg ("please enter a number!");
    }
  } else if (theEvent.isFrom (ch1_autoGain)) {
    // attempt to set gain so the received level==REF_DB
    if (gain1!=-666) {
      float n = gain1+(REF_DB-dBZSPL);
      if (n<0) {
        dbg (String.format("The input is %.1fdB too loud! I can't gain lower than zero\n", Math.abs(n)));
      } else if (n>40) {
        dbg (String.format("The input is too quiet! This would require %.1fdB of gain which is beyond my capabilities\n", Math.abs(n)));
      } else {
        dbg (String.format ("new gain=%.1f\n", n));
        String out = String.format("@gain1=%.1f\n", n);
        myPort.write(out);
      }
    } else {
      dbg ("haven't heard from the ESP32 yet\n");
    }
  }
  
  
  //channel 2 gain
  
  else if (theEvent.isFrom (ch2_gainUp)) {
    if (gain2!=-666) {
      float tempGain=gain2 +0.1;
      String out = String.format("@gain2=%.1f\n", tempGain);
      println (String.format ("OUT: %s", out));
      myPort.write(out);
    }
  } else if (theEvent.isFrom (ch2_gainDown)) {
    if (gain2!=-666) {
      float tempGain=gain2-0.1;
      String out = String.format("@gain2=%.1f\n", tempGain);
      println (String.format ("OUT: %s", out));
      myPort.write(out);
    }
  } else if (theEvent.isFrom (ch2_gainManual)) {
    try {
      float manualValue=Float.parseFloat (ch2_gainManual.getStringValue());
      if (manualValue >=0 && manualValue <=38) {
        manualValue = Math.round(manualValue*10)/10.0f;      // round to nearest 0.1
        dbg (String.format ("manual value: %.1f\n", manualValue));
        String out = String.format("@gain2=%.1f\n", manualValue);
        myPort.write(out);
      } else {
        dbg ("gain can only be 0-38dB\n");
      }
      ch2_gainManual.setFocus(false);
    } 
    catch (NumberFormatException e) {
      dbg ("please enter a number!");
    }
  } else if (theEvent.isFrom (ch2_autoGain)) {
    // attempt to set gain so the received level==REF_DB
    if (gain2!=-666) {
      float n = gain2+(REF_DB-dBZSPL);
      if (n<0) {
        dbg (String.format("The input is %.1fdB too loud! I can't gain lower than zero\n", Math.abs(n)));
      } else if (n>40) {
        dbg (String.format("The input is too quiet! This would require %.1fdB of gain which is beyond my capabilities\n", Math.abs(n)));
      } else {
        dbg (String.format ("new gain=%.1f\n", n));
        String out = String.format("@gain2=%.1f\n", n);
        myPort.write(out);
      }
    } else {
      dbg ("haven't heard from the ESP32 yet\n");
    }
  }
  
  
  else if (theEvent.isFrom (setInput1)) {
    myPort.write("@inputsel=1\n");
  } else if (theEvent.isFrom (setInput2)) {
    myPort.write("@inputsel=2\n");
  } else if (theEvent.isFrom (ESPReboot)) {
    println ("reboot");
    myPort.write("@reset=1\n");
  }
}

// hacks to find window location
PVector getWindowLocation() {
  PVector l = new PVector();

  java.awt.Frame f =  (java.awt.Frame) ((processing.awt.PSurfaceAWT.SmoothCanvas) surface.getNative()).getFrame();
  l.x = f.getX();
  l.y = f.getY();

  return l;
}

// set input button colours and draw an arrow
void setInputButtonColours() {
  if (selectedInput == 1) {
    setInput1.setColorBackground(0xffff00ff);
    setInput2.setColorBackground(0xff660066);
    //drawArrow (setInput1.getPosition()[0], setInput1.getPosition()[1], analysis.getPosition()[0] + analysis.getWidth()/2, analysis.getPosition()[1] + 20);
   
  } else {
    setInput1.setColorBackground(0xff660066);
    setInput2.setColorBackground(0xffff00ff);
  }
}

// draw an arrow
void drawArrow(int sx, int sy, int ex, int ey){
  line(sx,sy,ex, ey);
  float angle = atan2 (ey-sy,ex-sx);
  //println (degrees(angle));
  pushMatrix();
  translate (ex, ey);
  rotate(angle);
  line (0, 0,-10, -10);
  line (0,0,-10,10);
  popMatrix();
}




// print to the debug pane
public void dbg(String[] text) {
  for (int i=0; i<text.length; i++) {
    consoleTextArea.append(text[i]+"\n");
  }
  consoleTextArea.scroll(consoleTextArea.getText().length());
}

public void dbgClear() {
  consoleTextArea.clear();
}
