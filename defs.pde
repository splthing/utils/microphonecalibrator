static final float REF_DB = 94.0;

static final float iec_f[] = {10, 12.5, 16, 20, 25, 31.5, 40, 50, 63, 80, 
  100, 125, 160, 200, 250, 315, 400, 500, 630, 800, 
  1000, 1250, 1600, 2000, 2500, 3150, 4000, 5000, 6300, 8000, 
  10000, 12500, 16000, 20000};

// sweep params
static final int SWEEP_POINTS = iec_f.length;    // use the iec specified freqs for now
static final int SWEEP_DWELL = 500;      // milliseconds to wait. SPLthing takes 125ms to process a block.
 

static final float FREQ_MIN = 20;
static final float FREQ_MAX = 22050;

static final float CALIB_FREQ = 1000;

//// set min/max 
//static final float FREQ_MIN = 
//static final float FREQ_MAX = 22050;

static final float FREQ_STEP = (log(FREQ_MAX) - log (FREQ_MIN)) / (SWEEP_POINTS-1);

// use this if we want log frequencies between our specified min/max above
//public float getFreq(int point) {
//  if (point > (SWEEP_POINTS-1)) return 0;
//  return exp(log(FREQ_MIN) + point*FREQ_STEP);
//}

// use this for IEC specified freqs, just returns whatever out of the array
public float getFreq (int point) {
  if (point > (SWEEP_POINTS-1)) return 0;
  return iec_f[point];
}

class Measurement {
  public int step;
  public float freq;
  public float amp;
}
