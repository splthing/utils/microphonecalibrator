import grafica.*;
import processing.serial.*;
import controlP5.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.time.LocalDateTime;
import java.time.format.*;
import java.nio.file.*;
import com.jsyn.*;          
import com.jsyn.unitgen.*;

SecondApplet sa;
String[] args = {"TwoFrameTest"};

Synthesizer synth = JSyn.createSynthesizer();
AudioDeviceManager am = AudioDeviceFactory.createAudioDeviceManager();
ChannelOut myOutput;
ChannelOut myOutput2;
SineOscillator sineOsc;
GPlot plot;
GPlot plot2;
GPointsArray pointsRef = new GPointsArray(SWEEP_POINTS);
GPointsArray pointsTarget = new GPointsArray(SWEEP_POINTS);
GPointsArray pointsDiff = new GPointsArray(SWEEP_POINTS);
GPointsArray pointsCal = new GPointsArray(SWEEP_POINTS);

GPointsArray fPoints;    // never gets instantiated, just pointed to whichever other GPointsArray we want to dump results into.
Serial myPort;

PImage octavePlot;
float LFLow, LFHigh, HFLow, HFHigh;        // ranges for LF and HF filter generation
float dBFS, dBZSPL;
float gain1 = -666;
float gain2 = -666;
int heap=0;
String buf = "";
int selectedInput;
boolean sweepOn =false;
ArrayBlockingQueue<Measurement> q = new ArrayBlockingQueue<Measurement>(SWEEP_POINTS);
float ff;
float sineAmp;
boolean sineActive=false;

void setup() {
  prepareExitHandler();
  // find the soundcard
  int deviceID=-1;
  int deviceOutputs=0;
  for (int i=0; i< am.getDeviceCount(); i++) {
    println (String.format ("Audio device %2d: %s", i, am.getDeviceName(i)));
    if (am.getDeviceName(i).equals (DEVICE_NAME)) {
      deviceID=i;
      deviceOutputs=am.getMaxOutputChannels(i);
      print(String.format ("Found device %s with %d output channels\n", DEVICE_NAME, deviceOutputs));
      break;
    }
  }
  if (deviceID==-1) {
    print ("can't find the soundcard, exiting\n");
    return;
  }
  synth.start (48000, -1, 0, deviceID, deviceOutputs);      // init the soundcard, outputs only
  synth.add (myOutput = new ChannelOut());                  // add an output
  myOutput.setChannelIndex(AUDIO_OUTPUT);
  myOutput.start();

  synth.add (myOutput2 = new ChannelOut());                 // add another output
  myOutput2.setChannelIndex(AUDIO_OUTPUT_2);
  myOutput2.start();

  synth.add(sineOsc = new SineOscillator(1000, 0.0));
  sineOsc.start();

  sineOsc.output.connect (0, myOutput.input, 0);            // connect the oscillator to the outputs
  sineOsc.output.connect (0, myOutput2.input, 0);

  plot = new GPlot(this);
  plot2 = new GPlot(this);

  plot.setPos (10, 10);
  plot.setDim(width-100, 200);
  plot.setXLim (20, FREQ_MAX+3000);
  plot.setYLim (80, 120);
  plot.setLogScale ("x");

  // plot2 is right hand axis for the difference
  plot2.setPos(plot.getPos());
  plot2.setMar(plot.getMar());
  plot2.setDim(plot.getDim());
  plot2.setAxesOffset(4);
  plot2.setTicksLength(4);

  plot2.getRightAxis().setAxisLabelText("diff (dB)");
  plot2.getRightAxis().setNTicks(20);
  plot2.getRightAxis().setDrawTickLabels(true);
  plot2.setXLim (20, FREQ_MAX+3000);
  plot2.setYLim(-10, 10);
  plot2.setLogScale ("x");

  plot.getYAxis().getAxisLabel().setText("dBZ SPL");
  plot.getXAxis().getAxisLabel().setText("f");
  plot.getXAxis().setDrawTickLabels(true);
  plot.setLineWidth(3);
  plot.setPointColor(color(100, 100, 255, 50));
  plot.addLayer("ref", pointsRef);
  plot.getLayer("ref").setPointColor(color(255, 50, 50, 200));
  plot.getLayer("ref").setLineColor(color(255, 50, 50, 200));
  plot.getLayer("ref").setLineWidth(2);
  plot.addLayer("target", pointsTarget);
  plot.getLayer("target").setPointColor(color(50, 50, 255, 200));
  plot.getLayer("target").setLineColor(color(50, 50, 255, 200));
  plot.getLayer("target").setLineWidth(2);
  plot2.addLayer("diff", pointsDiff);
  plot2.getLayer("diff").setPointColor(color(50, 255, 50, 200));
  plot2.getLayer("diff").setLineColor(color(50, 255, 50, 200));
  plot2.getLayer("diff").setLineWidth(4);
  plot2.addLayer("cal", pointsCal);
  plot2.getLayer("cal").setPointColor(color(50, 255, 255, 50));
  plot2.getLayer("cal").setLineColor(color(50, 255, 255, 50));
  plot2.getLayer("cal").setLineWidth(4);
  plot2.setGridLineWidth(2);
  plot.activatePanning();
  plot.activateZooming(1.1, CENTER, CENTER);
  plot2.activatePanning();
  plot2.activateZooming(1.1, CENTER, CENTER);


  cp5init();
  // load the calibration sweep...
  pointsCal = loadSweep (dataPath(CAL_FILENAME), false);
  // load the ref and target for testing...
  pointsRef = loadSweep(dataPath(REF_FILENAME), true);
  pointsTarget = loadSweep("/Users/owen/Desktop/refs/MOTU ultralite Mk3 line out 10k imp.json", false);
  calcDiff();
  // load the preferences
  ls ();
  println ("opening serial...");
  myPort=new Serial (this, SERIAL_PORT, 115200);
}

void settings() {
  size (1024, 940);
}

void draw() {
  background(44);
  plot.beginDraw();
  plot.drawBackground();
  plot.drawBox();
  plot.drawXAxis();
  plot.drawYAxis();
  plot.drawLines();
  plot.drawTitle();
  plot.drawPoints();
  plot.drawGridLines(GPlot.BOTH);
  plot.endDraw();

  plot2.beginDraw();
  //plot2.drawBackground();
  //plot2.drawBox();
  //plot2.drawXAxis();
  //plot2.drawYAxis();
  plot2.drawLines();

  plot2.drawRightAxis();
  //plot2.drawTitle();
  plot2.drawPoints();
  plot2.drawGridLines(GPlot.HORIZONTAL);
  plot2.endDraw(); 


  // grab data coming from the sweep thread and put it in the relevant points array.
  if (q.size() > 0) {
    Measurement newM = q.remove();
    fPoints.add(newM.freq, newM.amp);
    //also calculate the diff
    if ((pointsTarget.getNPoints() > 0) && pointsRef.getNPoints() > 0) {
      pointsDiff.add (newM.freq, diff (newM.step));
    }
  }


  // draw cp5
  cp5.draw();

  // draw an arrow from the active channel to the analyzer
  strokeWeight (5);
  stroke(255);

  if (selectedInput==1) {
    drawArrow (100, 500, 200, 540);
  } else if (selectedInput==2) {
    drawArrow (300, 500, 200, 540);
  }

  fill(255);
  textSize(45);
  textAlign (CENTER, BASELINE);
  text (String.format ("%.1f dBFS", dBFS), analysis.getPosition()[0]+analysis.getWidth()/2, analysis.getPosition()[1] + 50);
  text (String.format ("%.1f dBZ SPL", dBZSPL), analysis.getPosition()[0]+analysis.getWidth()/2, analysis.getPosition()[1] + 100);
  textSize(30);
  //text (String.format ("(%.1f dBFS)", 20 * Math.log10 (calibrationLevel.getValue())), 780, 400);
  calibrationLevel.setValueLabel(String.format ("(%.1f dBFS)", 20 * Math.log10 (calibrationLevel.getValue())));
  //text (String.format ("ESP32 heap=%d", heap), 360, 350);

  // draw range slider values
  textSize (14);
  textAlign (CENTER, CENTER);
  text ("LOW FREQ FILTER", LFRange.getPosition()[0] + LFRange.getWidth()/2, LFRange.getPosition()[1]+LFRange.getHeight()/2 - 12);
  text ("HIGH FREQ FILTER", HFRange.getPosition()[0] + HFRange.getWidth()/2, HFRange.getPosition()[1]+HFRange.getHeight()/2 - 12);
  textSize (17);
  text (String.format ("%s - %s", formatFreq(LFLow), formatFreq(LFHigh)), LFRange.getPosition()[0] + LFRange.getWidth()/2, LFRange.getPosition()[1]+LFRange.getHeight()/2+5);
  text (String.format ("%s - %s", formatFreq(HFLow), formatFreq(HFHigh)), HFRange.getPosition()[0] + HFRange.getWidth()/2, HFRange.getPosition()[1]+HFRange.getHeight()/2+5);

  // show octave plot if exitst
  if (octavePlot!=null) {
    image (octavePlot, 520, 570, width-510-20, 380);
  }

  // draw points on chart

  if (applyCal.getValue() == 1) {
    if (pointsCal.getNPoints()!=0) {
      plot.getLayer("ref").setPoints(applyCal(pointsRef));
      plot.getLayer("target").setPoints(applyCal(pointsTarget));
    }
  } else {
    plot.getLayer("ref").setPoints(pointsRef);
    plot.getLayer("target").setPoints(pointsTarget);
  }

  plot2.getLayer("diff").setPoints(pointsDiff);    // difference on right axis (centered on 0)
  plot2.getLayer("cal").setPoints(pointsCal);
}

// apply the calibration array. the size of the specified points array could be in the middle of a sweep
// so we have to handle varying sizes of array
GPointsArray applyCal (GPointsArray p) {
  GPointsArray calibrated = new GPointsArray();
  for (int i=0; i<p.getNPoints(); i++) {
    calibrated.add (p.get(i).getX(), calDiff(p, i));
  }

  return calibrated;
}

// calculate the diference between a supplied point and the same point from calibration array
float calDiff (GPointsArray points, int i) {
  return points.get(i).getY() + pointsCal.get(i).getY();
}

void calcDiff() {
  // calculate all the difference points
  clearPoints (pointsDiff);
  float[] s = new float [SWEEP_POINTS];
  if (pointsRef.getNPoints() > 0 && pointsTarget.getNPoints() > 0) {
    for (int i=0; i<SWEEP_POINTS; i++) {
      s[i]=90.0;
      pointsDiff.add (pointsRef.get(i).getX(), diff(i), String.format("%fHz, $fdB", iec_f[i], diff(i)));
    }
    plot2.setPointSizes(s);
  }
}

float diff (int step) {
  //println (step);
  return pointsRef.get(step).getY()-pointsTarget.get(step).getY();
}

void clearPoints (GPointsArray points) {
  points.removeRange(0, points.getNPoints());
}


// handle shutdown properly
private void prepareExitHandler () {
  Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
    //Runtime.getRuntiem
    public void run () {
      System.out.println("SHUTDOWN HOOK");
      ss();
      myPort.stop();
    }
  }
  ));
}

// handle serial port input
void serialEvent(Serial myPort) {
  if ( myPort.available() > 0) {
    int b = myPort.read();
    if (b=='\n')
      process();
    else {
      buf+=(char)b;
    }
  }
}

// process a new line from serial port
void process() {
  try {
    if (buf.startsWith("*")) {
      String[] s = buf.split("=");
      String param = s[0].substring (1);            // get the parameter name, left of equals sign
      if (param.equals("PeakFS")) {
        dBFS = Float.parseFloat(s[1]);
      } else if (param.equals("SPLZF")) {
        dBZSPL = Float.parseFloat(s[1]);
      } else if (param.equals("gain1")) {
        if (!ch1_gainManual.isFocus()) ch1_gainManual.setText(s[1]);
        gain1 = Float.parseFloat (s[1]);
      } else if (param.equals("gain2")) {
        if (!ch2_gainManual.isFocus()) ch2_gainManual.setText(s[1]);
        gain2 = Float.parseFloat (s[1]);
      } else if (param.equals("HEAP")) {
        heap = Integer.parseInt (s[1]);
      } else if (param.equals("raw1s")) {
        println (s[1]);
      } else if (param.equals("inputsel")) {
        // change the colours of the buttone
        selectedInput=Integer.parseInt(s[1]);
        setInputButtonColours();
     
      }
    } else {   // it's not a parameter, so just display the line in the log window.
      dbg("ESP32: "+buf+"\n");    // we stripped \n earlier so add it back
    }
  } 
  catch (Exception e) {
    // likely just junk serial, ignore it
    println (String.format ("bad shit: %s", buf));
  }

  buf="";
}
