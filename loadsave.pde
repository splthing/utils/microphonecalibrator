// load and save settings

void saveSettings (String filename) {
  JSONObject j = new JSONObject();
  j.setFloat ("calibrationLevel", calibrationLevel.getValue());
  j.setFloat ("applyCal", applyCal.getValue());

  PVector windowLocation = getWindowLocation();
  j.setInt ("x", (int)windowLocation.x);
  j.setInt ("y", (int)windowLocation.y);
  saveJSONObject(j, filename);
}

void loadSettings (String filename) {
  try {
    JSONObject j = loadJSONObject(filename);
    calibrationLevel.setValue(j.getFloat("calibrationLevel"));
    applyCal.setValue (j.getFloat ("applyCal"));
    int x=0, y=0;
    try {
      x = j.getInt("x");
      y = j.getInt("y");
    } 
    catch (RuntimeException e) {
    }
    surface.setLocation (x, y);
  } 
  catch (Exception e) {
    println ("something went wrong loading the settings file!");
  }
}


void saveTarget(File selection) {
  // save da thing
  if (selection!=null) { 
    saveSweep ("target sweep", selection.getAbsolutePath(), pointsTarget);
    println (selection.getAbsolutePath());
  }
}

void loadTarget(File selection) {
  if (selection !=null) {
    pointsTarget = loadSweep(selection.getAbsolutePath(), false);
    println (selection.getAbsolutePath());
    // recalc
    calcDiff();
  }
}

// save a sweep file

void saveSweep(String remark, String filename, GPointsArray points) {
  JSONObject j = new JSONObject() ;
  String dtStr = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
  j.setString ("remark", remark);
  j.setInt("SWEEP_POINTS", SWEEP_POINTS);
  j.setInt("SWEEP_DWELL", SWEEP_DWELL);
  j.setFloat("SWEEP_AMP", sineAmp);
  j.setFloat("FREQ_MIN", FREQ_MIN);
  j.setFloat("FREQ_MAX", FREQ_MAX);
  j.setFloat ("FREQ_STEP", FREQ_STEP);
  j.setFloat ("LF_MIN", LFLow);
  j.setFloat ("LF_MAX", LFHigh);
  j.setFloat ("HF_MIN", HFLow);
  j.setFloat ("HF_MAX", HFHigh);
  j.setString ("date", dtStr);
  j.setJSONArray ("points", points2JSONArray (points));
  saveJSONObject(j, filename);
}

// load a sweep file and return the points within. Optionally we can restore the tone generator level
// to the amplitude stored in the file.

GPointsArray loadSweep(String filename, boolean restoreConfig) {
  JSONObject j;
  try {
    j = loadJSONObject (filename);
  } 
  catch (Exception e) {
    return new GPointsArray();    // if no file found, just return a blank array
  }
  JSONArray a = j.getJSONArray ("points");
  JSONObject measurement;
  int pointsCount = a.size();
  GPointsArray p = new GPointsArray(pointsCount);
  println (String.format ("%d points to load", pointsCount));
  for (int i=0; i<pointsCount; i++) {
    measurement = a.getJSONObject(i);
    p.add(measurement.getFloat("freq"), measurement.getFloat ("amp"));
  }
  try { 
    LFRange.setArrayValue(new float[]  {j.getFloat("LF_MIN"), j.getFloat("LF_MAX")});
    HFRange.setArrayValue(new float[]  {j.getFloat("HF_MIN"), j.getFloat("HF_MAX")});
  } 
  catch (Exception e) {
    //whatever
  }
  if (restoreConfig) {
    // we will only be doing this if loading a reference sweep
    calibrationLevel.setValue(j.getFloat("SWEEP_AMP"));
  }

  return p;
}

// convert points to JSON for saving
JSONArray points2JSONArray (GPointsArray points) {
  JSONObject j;
  JSONArray a = new JSONArray();
  for (int i=0; i<points.getNPoints(); i++) {
    GPoint p= points.get(i);
    j = new JSONObject();
    j.setFloat ("freq", p.getX());
    j.setFloat ("amp", p.getY());
    a.append(j);
  }
  return a;
}

// callback methods that just supply the filename
void ss () {
  saveSettings (dataPath("settings.json"));
}

void ls () {
  loadSettings (dataPath("settings.json"));
}
