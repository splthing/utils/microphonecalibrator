void generateOctaveParams() {
  octavePlot = null;
  float temp;
  // gen
  dbg ("Generating filter params...\n");
  // first output the whole sweep as an array for the octave graph
  int lineCounter=0;
  String s = "ds_dB = ["; 
  for (int i=0; i<SWEEP_POINTS; i++) {
    temp = pointsDiff.get(i).getY();
    if (applyCal.getValue() == 1) temp+=pointsCal.get(i).getY();    // add the calibration value for this freq
    s+=String.format("%+.1f, ", -temp);
    if (++lineCounter == 10) {
      s+="...\n"; 
      lineCounter=0;
    }
  }
  if (s.substring(s.length()-2) .equals( ", ")) s=s.substring (0, s.length()-2);     // chop a trailing comma
  s+="];";

  println (s+"\n");
  String ds_l_f="ds_l_f  = [";
  String ds_l_dB="ds_l_dB = [";
  String ds_h_f="ds_h_f  = [";
  String ds_h_dB="ds_h_dB = [";

  int start, end;

  start = findIndex (iec_f, LFLow);
  end = findIndex (iec_f, LFHigh);

  for (int i=start; i<end; i++) {
    ds_l_f+=String.format ("%.1f, ", pointsDiff.get(i).getX());
    temp=pointsDiff.get(i).getY();
    if (applyCal.getValue() == 1) temp+=pointsCal.get(i).getY();    // add cal value if we want
    ds_l_dB+=String.format ("%+2.1f, ", -temp);
  }
  ds_l_f=chopTrailingComma (ds_l_f)+"];";
  ds_l_dB=chopTrailingComma (ds_l_dB)+"];";
  println (ds_l_f);
  println (ds_l_dB);

  start = findIndex (iec_f, HFLow);
  end = findIndex (iec_f, HFHigh);

  for (int i=start; i<end; i++) {
    ds_h_f+=String.format ("%.1f, ", pointsDiff.get(i).getX());
    temp=pointsDiff.get(i).getY();
    if (applyCal.getValue() == 1) temp+=pointsCal.get(i).getY();    // add cal value if we want
    ds_h_dB+=String.format ("%+2.1f, ", -temp);
  }
  ds_h_f=chopTrailingComma (ds_h_f)+"];";
  ds_h_dB=chopTrailingComma (ds_h_dB)+"];";
  println (ds_h_f);
  println (ds_h_dB);
  dbg ("Loading template file...\n");
  // now load the template file.
  Path templateFile = Paths.get(TEMPLATE_PATH);
  String filterSpecs = s+"\n"+ds_l_f+"\n"+ds_l_dB+"\n"+ds_h_f+"\n"+ds_h_dB+"\n\n\n";
  String outputImage = IMG_OUT_DIR+"/wahoo.jpg";
  try {
    String contents = new String(Files.readAllBytes(templateFile));
    dbg ("Inserting response...\n");
    //println (filterSpecs);
    //search and replace baby
    String newOutput = contents.replace ("%FILTERSPEC%", filterSpecs);
    newOutput = newOutput.replace ("%IMAGEFILENAME%", outputImage);
    newOutput = newOutput.replace ("%MEASUREMENTID%", OCTAVE_MICNAME);
    newOutput = newOutput.replace ("%TITLETEXT%", "["+OCTAVE_MICNAME+"] "+ LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
    PrintWriter outputFile = createWriter(IMG_OUT_DIR+"/out.m");
    dbg ("Writing output file...\n");
    outputFile.println (newOutput);
    outputFile.close();

    // now exec octave
    StringList stdout = new StringList();
    StringList stderr = new StringList();
    dbg ("Executing "+OCTAVE_PATH + "\n");
    exec (stdout, stderr, OCTAVE_PATH, IMG_OUT_DIR+"/out.m");

    //printArray(stderr);
    octavePlot = loadImage (IMG_OUT_DIR+"/wahoo.jpg");

    // get the gain, it's the first line
    double coGain = Double.parseDouble( split (stdout.get(0), "=")[1]);
    println (coGain);

    double[] sos = new double[stdout.size()];
    // walk through the rest of the stringlist and pull out the sos coeffs
    for (int i=1; i<stdout.size(); i++) {
      sos[i] = Double.parseDouble(stdout.get(i));
      println (String.format ("%d :: %.20f", i, sos[i]));
    }

    // generate an aray in the right order to send
    double[] co_sos = new double[] { sos[3], sos[5], -sos[9], -sos[11], sos[4], sos[6], -sos[10], -sos[12] };

    String sosCmd = "@coeff=";

    sosCmd+=String.format("%.16f", coGain);

    for (int i=0; i<co_sos.length; i++) {
      sosCmd+=String.format(",%.16f", co_sos[i]);
    }
    sosCmd+="\n";
    //myPort.write(sosCmd);

    println (sosCmd);

    //
  } 
  catch (Exception e) {
    exit();
  }
}

String chopTrailingComma (String s) {
  if (s.substring(s.length()-2) .equals( ", ")) return s.substring (0, s.length()-2);     // chop a trailing comma
  return s;
}
//ds_l_f  = [ 10,  20,   50,  100, 1000];
//ds_l_dB = [-26, -12, -3.5, -1.5,    0];
//ds_h_f  = [ 1000, 5000, 10000, 20000];
//ds_h_dB = [    0, +1.1,  +3.1, +14.0];

//ds_dB = [ -26,  -22,  -17,  -13,  -10,   -8,   -6,   -4, -2.8, -2.1, ...
//         -1.8, -1.4, -1.0, -0.5, -0.5,    0,    0,    0,    0,    0, ...
//            0,   0,     0,    0,    0, +0.5, +0.6, +1.1, +1.8, +2.5, ...
//         +3.1, +4.5, +8.0, +14.0];
