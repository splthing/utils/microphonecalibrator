public class SecondApplet extends PApplet {
  private ControlP5 cpp5;
  PImage img;
  public void settings() {
    size(1024, 768);

  }
  public void setup() {

    if (cpp5==null) cpp5 = new ControlP5(this);

  }
  public void draw() {
    background(255);
    if (img!=null) {
      image (img, 0, 0);
    }
  }
  public void pos (int x, int y) {
    surface.setLocation (x, y);
  }
  
  public void showCoeff () {
    // load the image
    
    img = loadImage(IMG_OUT_DIR+"/wahoo.jpg");
    image (img, 0, 0);
  }
  

  // override these methods to stop the whole sketch shutting down when this window is closed
  public void end() {
  }
  public void exitActual() {
    sa=null;    // ??
  }
  public void dispose() {
  }
}
