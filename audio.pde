
void sweepThread() {
  int freqStep = 0;
  float freq = getFreq(freqStep);

  Measurement m = new Measurement();
  sineOsc.frequency.set(freq);
  sineOn();                   // turn on generator
  delay(200);                    // wait for "things to settle down"
  while (freqStep < SWEEP_POINTS && sweepOn) {
    sineOsc.frequency.set(freq);
    delay(SWEEP_DWELL);          // wait for "things to settle down"
    m.freq=freq;
    m.step=freqStep;
    m.amp=dBZSPL;
    q.add(m);
    //print (String.format ("recorded %f dBFS at %fHz\n", dBFS, freq));
    freqStep++;
    freq=getFreq(freqStep);
  }
  sineOff();                    // turn off generator
  sweepOn=false;
}

void sineOn() {
  sineActive=true;
  sineOsc.amplitude.set(sineAmp);
}

void sineUpdate() {
  if (sineActive) sineOsc.amplitude.set(sineAmp);
}

void sineOff() {
  sineActive=false;
  sineOsc.amplitude.set(0);
}

String formatFreq(float f) {
  if (f<1000)
    return String.format ("%dHz", (int)f);
  else
    return String.format ("%.1fkHz", f/1000);
}
public void dbg(String text) {
  if (consoleTextArea.getText().length() > 65000) dbgClear();
  consoleTextArea.append(text);
  consoleTextArea.scroll(consoleTextArea.getText().length());
}
