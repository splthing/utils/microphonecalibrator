//what
//static final String DEVICE_NAME = "MOTU UltraLite mk3 Hybrid";
static final String DEVICE_NAME = "Loop 1 (2ch)";
static final int AUDIO_OUTPUT = 0;        // starts from 0
static final int AUDIO_OUTPUT_2 = 1;
//static final String SERIAL_PORT = "/dev/cu.SLAB_USBtoUART";
static final String SERIAL_PORT = "/dev/cu.usbserial";
static final String REF_FILENAME = "ref.json";
static final String CAL_FILENAME = "cal.json";

// stuff for shelling out to Octave
//static final String OCTAVE_PATH = "/Applications/Octave-6.3.0.app/Contents/Resources/usr/Cellar/octave-octave-app@6.3.0/6.3.0/bin/octave";
static final String OCTAVE_PATH = "/opt/local/bin/octave";
static final String TEMPLATE_PATH = "/Users/owen/Google Drive/My Drive/splthing/octave/template/template.m";
//static final String TEMPLATE_PATH = "template.m";

static final String IMG_OUT_DIR = "/Users/owen/Google Drive/My Drive/splthing/octave/working";
static final String OCTAVE_MICNAME = "Cranium";
